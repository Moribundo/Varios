// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("accessibility.typeaheadfind.flashBar", 0);
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1655993659);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1655993419);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1655993539);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1655895375);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1655993779);
user_pref("browser.bookmarks.defaultLocation", "2kFMfDz1DcNA");
user_pref("browser.bookmarks.editDialog.confirmationHintShowCount", 3);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.contentblocking.category", "strict");
user_pref("browser.download.dir", "/home/zx80/descargas");
user_pref("browser.download.folderList", 2);
user_pref("browser.download.panel.shown", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.avif", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.webp", true);
user_pref("browser.engagement.ctrlTab.has-used", true);
user_pref("browser.engagement.downloads-button.has-used", true);
user_pref("browser.engagement.home-button.has-used", true);
user_pref("browser.migration.version", 125);
user_pref("browser.newtabpage.activity-stream.impressionId", "{1bb587a7-1d88-45b7-98a9-4817b30d92f5}");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.open.lastDir", "/home/zx80/nextcloud/proyectos/miweb/so");
user_pref("browser.pageActions.persistedActions", "{\"ids\":[\"bookmark\",\"canvasblocker_kkapsner_de\"],\"idsInUrlbar\":[\"canvasblocker_kkapsner_de\",\"bookmark\"],\"idsInUrlbarPreProton\":[],\"version\":1}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.policies.applied", true);
user_pref("browser.policies.runOncePerModification.addSearchEngines", "/S92rzfUdA8IqLNkEq93Ow==");
user_pref("browser.policies.runOncePerModification.extensionsInstall", "[\"https://addons.mozilla.org/firefox/downloads/file/3933192/ublock_origin-1.42.4-an+fx.xpi\"]");
user_pref("browser.policies.runOncePerModification.extensionsUninstall", "[\"google@search.mozilla.org\",\"bing@search.mozilla.org\",\"amazondotcom@search.mozilla.org\",\"ebay@search.mozilla.org\",\"twitter@search.mozilla.org\"]");
user_pref("browser.policies.runOncePerModification.removeSearchEngines", "[\"Google\",\"Bing\",\"Amazon.com\",\"eBay\",\"Twitter\"]");
user_pref("browser.policies.runOncePerModification.setDefaultSearchEngine", "DuckDuckGo");
user_pref("browser.preferences.defaultPerformanceSettings.enabled", false);
user_pref("browser.proton.toolbar.version", 3);
user_pref("browser.safebrowsing.provider.mozilla.gethashURL", "");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1648639529943");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1648661129943");
user_pref("browser.safebrowsing.provider.mozilla.updateURL", "");
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20220327152814");
user_pref("browser.shell.checkDefaultBrowser", true);
user_pref("browser.shell.mostRecentDateSetAsDefault", "1655993394");
user_pref("browser.startup.couldRestoreSession.count", 1);
user_pref("browser.startup.lastColdStartupCheck", 1655993390);
user_pref("browser.theme.toolbar-theme", 0);
user_pref("browser.toolbars.bookmarks.showOtherBookmarks", false);
user_pref("browser.toolbars.bookmarks.visibility", "always");
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"home-button\",\"forward-button\",\"downloads-button\",\"stop-reload-button\",\"urlbar-container\",\"save-to-pocket-button\",\"fxa-toolbar-menu-button\",\"ublock0_raymondhill_net-browser-action\",\"_b86e4813-687a-43e6-ab65-0bde4ab75758_-browser-action\",\"cookieautodelete_kennydo_com-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\",\"jid1-ktlzuoiikvffew_jetpack-browser-action\",\"canvasblocker_kkapsner_de-browser-action\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"developer-button\",\"ublock0_raymondhill_net-browser-action\",\"_b86e4813-687a-43e6-ab65-0bde4ab75758_-browser-action\",\"cookieautodelete_kennydo_com-browser-action\",\"_b9db16a4-6edc-47ec-a1f4-b86292ed211d_-browser-action\",\"jid1-ktlzuoiikvffew_jetpack-browser-action\",\"canvasblocker_kkapsner_de-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"PersonalToolbar\",\"toolbar-menubar\",\"TabsToolbar\"],\"currentVersion\":17,\"newElementCount\":3}");
user_pref("browser.urlbar.suggest.bookmark", false);
user_pref("browser.urlbar.suggest.history", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("browser.urlbar.suggest.topsites", false);
user_pref("devtools.everOpened", true);
user_pref("devtools.theme.show-auto-theme-info", false);
user_pref("devtools.toolsidebar-height.inspector", 350);
user_pref("devtools.toolsidebar-width.inspector", 700);
user_pref("devtools.toolsidebar-width.inspector.splitsidebar", 350);
user_pref("distribution.iniFile.exists.appversion", "100.0.2");
user_pref("distribution.iniFile.exists.value", true);
user_pref("distribution.io.gitlab.librewolf-community.bookmarksProcessed", true);
user_pref("dom.disable_open_during_load", false);
user_pref("dom.forms.autocomplete.formautofill", true);
user_pref("dom.push.userAgentID", "854a34bc752a4e3d85b09309aaa505f0");
user_pref("dom.security.https_only_mode", false);
user_pref("dom.security.https_only_mode_ever_enabled", true);
user_pref("extensions.activeThemeID", "{f07f0b4d-dfc9-484b-9f63-65d66b7b117c}");
user_pref("extensions.blocklist.pingCountVersion", -1);
user_pref("extensions.databaseSchema", 35);
user_pref("extensions.lastAppBuildId", "20220521145420");
user_pref("extensions.lastAppVersion", "100.0.2");
user_pref("extensions.lastPlatformVersion", "100.0.2");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.pictureinpicture.enable_picture_in_picture_overrides", true);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.ui.dictionary.hidden", false);
user_pref("extensions.ui.extension.hidden", false);
user_pref("extensions.ui.lastCategory", "addons://list/extension");
user_pref("extensions.ui.locale.hidden", false);
user_pref("extensions.ui.sitepermission.hidden", true);
user_pref("extensions.ui.theme.hidden", false);
user_pref("extensions.webcompat.enable_shims", true);
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.CanvasBlocker@kkapsner.de", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.CookieAutoDelete@kennydo.com", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.uBlock0@raymondhill.net", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.{b86e4813-687a-43e6-ab65-0bde4ab75758}", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.{b9db16a4-6edc-47ec-a1f4-b86292ed211d}", true);
user_pref("extensions.webextensions.uuids", "{\"formautofill@mozilla.org\":\"fc6856a2-1091-4fc8-a417-f0416621791a\",\"pictureinpicture@mozilla.org\":\"61ee5ce0-68c0-44a3-8737-9e7b5fc6cff3\",\"screenshots@mozilla.org\":\"fecb1a5d-aee1-4333-8902-43a6841f66e7\",\"webcompat@mozilla.org\":\"e0b0c36f-0abc-42c2-86d1-10fcf40cfd81\",\"default-theme@mozilla.org\":\"9d65fda6-970b-49bb-890e-4d3096b5ba0a\",\"addons-search-detection@mozilla.com\":\"d41b760c-f56c-40fb-8475-7e453155128f\",\"uBlock0@raymondhill.net\":\"c786e255-d2c4-478e-818f-9ea7386d6785\",\"wikipedia@search.mozilla.org\":\"10f76690-5a0f-4063-b978-f6e3f4699905\",\"ddg@search.mozilla.org\":\"d5e5840f-b187-4a51-9ae3-eb29131c0b9f\",\"{f07f0b4d-dfc9-484b-9f63-65d66b7b117c}\":\"04ada906-59d6-4cf6-a598-e84c26646b1d\",\"{b86e4813-687a-43e6-ab65-0bde4ab75758}\":\"93f25415-39d4-4539-a6b4-32ba3e52763e\",\"CookieAutoDelete@kennydo.com\":\"f7773b80-c729-4b2f-85d5-0ea814067fb1\",\"{b9db16a4-6edc-47ec-a1f4-b86292ed211d}\":\"c411b450-e68c-4b21-9016-67ef16374ce1\",\"CanvasBlocker@kkapsner.de\":\"c52ff7e4-fd6d-4c96-9fa5-00bbab0a2ea0\"}");
user_pref("fission.experiment.max-origins.last-disqualified", 0);
user_pref("fission.experiment.max-origins.last-qualified", 1648639540);
user_pref("fission.experiment.max-origins.qualified", true);
user_pref("gecko.handlerService.defaultHandlersVersion", 1);
user_pref("gfx.blacklist.webrender", 4);
user_pref("gfx.blacklist.webrender.failureid", "FEATURE_FAILURE_DDX_INTEL");
user_pref("idle.lastDailyNotification", 1655895595);
user_pref("intl.accept_languages", "en-US, en");
user_pref("intl.locale.requested", "es-ES,en-US");
user_pref("javascript.use_us_english_locale", true);
user_pref("layout.spellcheckDefault", 0);
user_pref("media.gmp-manager.buildID", "20220521145420");
user_pref("media.gmp-manager.lastCheck", 1655993893);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("media.peerconnection.ice.no_host", false);
user_pref("media.videocontrols.picture-in-picture.video-toggle.has-used", true);
user_pref("network.cookie.lifetimePolicy", 0);
user_pref("network.http.referer.disallowCrossSiteRelaxingDefault.top_navigation", true);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("places.database.lastMaintenance", 1655743108);
user_pref("places.history.enabled", false);
user_pref("pref.general.disable_button.default_browser", false);
user_pref("pref.privacy.disable_button.cookie_exceptions", false);
user_pref("pref.privacy.disable_button.tracking_protection_exceptions", false);
user_pref("privacy.annotate_channels.strict_list.enabled", true);
user_pref("privacy.cpd.cookies", false);
user_pref("privacy.cpd.offlineApps", true);
user_pref("privacy.history.custom", true);
user_pref("privacy.partition.network_state.ocsp_cache", true);
user_pref("privacy.purge_trackers.date_in_cookie_database", "0");
user_pref("privacy.sanitize.pending", "[{\"id\":\"shutdown\",\"itemsToClear\":[\"cache\",\"cookies\",\"offlineApps\",\"history\",\"formdata\",\"downloads\",\"sessions\"],\"options\":{}},{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]");
user_pref("privacy.spoof_english", 1);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.socialtracking.enabled", true);
user_pref("privacy.userContext.extension", "CanvasBlocker@kkapsner.de");
user_pref("reader.color_scheme", "light");
user_pref("security.disable_button.openCertManager", false);
user_pref("security.sandbox.content.tempDirSuffix", "0d8f58cf-99c7-4b53-9afd-0359c8821f35");
user_pref("security.ssl.require_safe_negotiation", false);
user_pref("services.sync.engine.addresses.available", true);
user_pref("signon.generation.enabled", false);
user_pref("signon.management.page.breach-alerts.enabled", false);
user_pref("storage.vacuum.last.index", 1);
user_pref("storage.vacuum.last.places.sqlite", 1654184380);
user_pref("toolkit.startup.last_success", 1655993387);
user_pref("toolkit.telemetry.cachedClientID", "46ff6b43-6249-48f1-9e2d-58fedf6f07ab");
user_pref("ui.key.menuAccessKeyFocuses", false);
